// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the HIDDEN_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// HIDDEN_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef HIDDEN_EXPORTS
#define HIDDEN_API __declspec(dllexport)
#else
#define HIDDEN_API __declspec(dllimport)
#endif


// static sample image
extern HIDDEN_API const int imagexs;
extern HIDDEN_API const int imageys;

extern HIDDEN_API const unsigned char imagedata[240*320];

// best ASM filter
HIDDEN_API void Classic_ASM(
	unsigned char* dst,       // IN: buffer for result
	const unsigned char* src, // IN: buffer for source
	int xs,                   // IN: ROI size
	int ys,                   //
	void* buf,                // IN: buffer
	int str);                 // IN: image stride

// 'classic' C filter 
HIDDEN_API void Classic_C(
	unsigned char* dst,       // IN: buffer for result
	const unsigned char* src, // IN: buffer for source
	int xs,                   // IN: image size
	int ys,                   //
	int* buf1);               // IN: buffer for temporary vars

// best C filter
HIDDEN_API void Best_C(
	unsigned char* dst,       // IN: buffer for result
	const unsigned char* src, // IN: buffer for source
	int xs,                   // IN: image size
	int ys,                   //
	int* buf1,                // IN: buffer for temporary vars
	unsigned char* buf2);     // IN: buffer for temporary vars

