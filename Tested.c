/*-----------------------------------------------------------------------*/
/*                                                                       */
/*      Created:  6 February 2001                                        */
/*      Modified: ?                                                      */
/*      Revision: x.0.00                                                 */
/*      Purpose:  You should change the TestedFilter function            */
/*      Purpose:  placeholder to your code                               */
/*      Authors:                                                         */
/*        Ivan Matveev (primary)                                         */
/*        YOU                                                            */
/*                                                                       */
/*-----------------------------------------------------------------------*/
  #include <emmintrin.h>
#define STEP 16
#define NUM_16BIT_IN_128 8
#define NUM_8BIT_IN_128 16


#ifndef MIN
#define  MIN(a,b) (((a) < (b)) ? (a) : (b))
#endif

#ifndef MAX
#define  MAX(a,b) (((a) > (b)) ? (a) : (b))
#endif

/// � �������� ������ src ������ ���� ����������� �� ���� ������� ������, 
/// ��� ���� � ��������. ��� ����� ��� ������������� ��������� ��������� ������
/// ��������� ����� bufferTmp ������ ���� �������� xs+1
void TestedFilter( /// SSE
                         unsigned char* dst,       // IN: buffer for result
                         const unsigned char* src, // IN: buffer for source
                         int xs,                   // IN: image size
                         int ys,
                         unsigned short * bufferTmp,
                         unsigned char * ref )                   //
{
    int i,j;

    __declspec(align(16)) short sum_8[STEP];        /// ���� ���,������������, ����� ������
    __declspec(align(16)) short center_8[STEP];


    __m128i const25;

    for( i = 0; i < NUM_16BIT_IN_128; ++ i ) const25.m128i_u16[i] = 25;

    /// ������� ����� �� ��������
    for( i = 0; i < xs; ++ i )
    {
        bufferTmp[i] = 
            src[(0)*xs+(i)] +
            src[(1)*xs+(i)] +   
            src[(2)*xs+(i)] +   
            src[(3)*xs+(i)] +   
            src[(4)*xs+(i)];
    }


    /// ���� ������� ����� src �� ����� ������ ������ � �����, �� 
    /// ����� ���������� ���� ���� �� ys-3 � �������� ��������� ��������� 
    /// ��� ��������� �������
    for (j=2;j<ys-2;j++)
    {
        unsigned int sumCur = bufferTmp[0] + bufferTmp[1] + bufferTmp[2] + bufferTmp[3] + bufferTmp[4];
        const unsigned char * pixelSrc = &src[j*xs+2];
        const unsigned char * pixelSrcLeft2Up2 = &src[(j-2)*xs+0];
        //const unsigned char * pixelSrcRight3Up2 = &src[(j+3)*xs+0];
        int shift = xs*5;
        unsigned char * pixelDst = &dst[j*xs+2];

        for (i=2;i<xs-STEP-2;i+=STEP)
        {
            int k;
            for( k = 0; k < STEP; ++ k )
            {
                center_8[k] = *pixelSrc++;
                sum_8[k] = sumCur;

                /// ��������� ����� �������� 
                sumCur += (bufferTmp[i+k+3] - bufferTmp[i+k-2]);

                /// ��������� ����� �� -2-�� �������
                //bufferTmp[i-2] -= ; //src[ (j-2)*xs + (i-2) ];
                bufferTmp[i+k-2] = bufferTmp[i+k-2] + pixelSrcLeft2Up2[shift] - pixelSrcLeft2Up2[0]; //src[ (j+3)*xs + (i-2) ];
                pixelSrcLeft2Up2++;
            }
                
            
            {
                __m128i *center_intrin = (__m128i*)center_8;    // ����� �� 8-�� 16-������� �������� ����� (���������)
                __m128i *sum_intrin = (__m128i*)sum_8;          // ����� �� 8-�� 16-������� �������� ����� (���������)
                
                __m128i center_mul_25_0 = _mm_mullo_epi16( center_intrin[0], const25 ); // �������� �� 25 (8 16-������� �������� �� 8 16-������� ��������, ������� ����� ����������
                __m128i center_mul_25_sub_sum_0 = _mm_subs_epu16 ( center_mul_25_0, sum_intrin[0] );  /// �������� ��� ����������� � ����������, �� ���� ������������� ���������� ����� ����������. 

                __m128i center_mul_25_1 = _mm_mullo_epi16( center_intrin[1], const25 ); // �������� �� 25 (8 16-������� �������� �� 8 16-������� ��������, ������� ����� ����������
                __m128i center_mul_25_sub_sum_1 = _mm_subs_epu16 ( center_mul_25_1, sum_intrin[1] );  /// �������� ��� ����������� � ����������, �� ���� ������������� ���������� ����� ����������. 

                __m128i center_mul_25_sub_sum_res = _mm_packus_epi16( center_mul_25_sub_sum_0, center_mul_25_sub_sum_1 ); // ������ 16-������ �������� ����� � 8-������ ����������� � ���������� (�� ���� �������� �� 255)
                for( k = 0; k < STEP; ++ k )
                {
                    *pixelDst++ = center_mul_25_sub_sum_res.m128i_u8[k]; 
                }
            }
           

        }
        for (;i<xs-2;i++)
        {
            short x;
            short center = *pixelSrc; //src[j*xs+i];
            /// ������� �����
            short sum = sumCur;

            /// ��������� ����� �������� 
            sumCur += (bufferTmp[i+3] - bufferTmp[i-2]);

            /// ��������� ����� �� -2-�� �������
            //bufferTmp[i-2] -= ; //src[ (j-2)*xs + (i-2) ];
            bufferTmp[i-2] = bufferTmp[i-2] + pixelSrcLeft2Up2[shift] - pixelSrcLeft2Up2[0]; //src[ (j+3)*xs + (i-2) ];
            pixelSrcLeft2Up2++;
            x = 25*center-sum;
            if (x>255)
                x = 255;
            else
                if (x<0)
                    x = 0;
            *pixelDst = x; //dst[j*xs+i] = x;

            pixelDst++;
            pixelSrc++;
        }
        for( i = xs-4; i < xs; ++ i )
        {
            bufferTmp[i] -= src[ (j-2)*xs + (i) ];
            bufferTmp[i] += src[ (j+3)*xs + (i) ];
        }

    }

    //dst[2*xs+3] = 111;
}

/// � �������� ������ src ������ ���� ����������� �� ���� ������� ������, 
/// ��� ���� � ��������. ��� ����� ��� ������������� ��������� ��������� ������
/// ��������� ����� bufferTmp ������ ���� �������� xs+1
void TestedFilter_No_SSE(
                  unsigned char* dst,       // IN: buffer for result
                  const unsigned char* src, // IN: buffer for source
                  int xs,                   // IN: image size
                  int ys,
                  unsigned short * bufferTmp )                   //
{
    unsigned int center,sum=0;
    int x,i,j;

    /// ������� ����� �� ��������
    for( i = 0; i < xs; ++ i )
    {
        bufferTmp[i] = 
            src[(0)*xs+(i)] +
            src[(1)*xs+(i)] +   
            src[(2)*xs+(i)] +   
            src[(3)*xs+(i)] +   
            src[(4)*xs+(i)];
    }

    /// ���� ������� ����� src �� ����� ������ ������ � �����, �� 
    /// ����� ���������� ���� ���� �� ys-3 � �������� ��������� ��������� 
    /// ��� ��������� �������
    for (j=2;j<ys-2;j++)
    {
        unsigned int sumCur = bufferTmp[0] + bufferTmp[1] + bufferTmp[2] + bufferTmp[3] + bufferTmp[4];
        const unsigned char * pixelSrc = &src[j*xs+2];
        const unsigned char * pixelSrcLeft2Up2 = &src[(j-2)*xs+0];
        //const unsigned char * pixelSrcRight3Up2 = &src[(j+3)*xs+0];
        int shift = xs*5;
        unsigned char * pixelDst = &dst[j*xs+2];

        for (i=2;i<xs-2;i++)
        {
            center = *pixelSrc; //src[j*xs+i];
            /// ������� �����
            sum = sumCur;

            /// ��������� ����� �������� 
            sumCur += (bufferTmp[i+3] - bufferTmp[i-2]);

            /// ��������� ����� �� -2-�� �������
            //bufferTmp[i-2] -= ; //src[ (j-2)*xs + (i-2) ];
            bufferTmp[i-2] = bufferTmp[i-2] + pixelSrcLeft2Up2[shift] - pixelSrcLeft2Up2[0]; //src[ (j+3)*xs + (i-2) ];
                pixelSrcLeft2Up2++;
            x = 25*center-sum;
            if (x>255)
                x = 255;
            else
                if (x<0)
                    x = 0;
            *pixelDst = x; //dst[j*xs+i] = x;

            pixelDst++;
            pixelSrc++;
        }
        for( i = xs-4; i < xs; ++ i )
        {
            bufferTmp[i] -= src[ (j-2)*xs + (i) ];
            bufferTmp[i] += src[ (j+3)*xs + (i) ];
        }

    }
}


void TestedFilter_original(
                  unsigned char* dst,       // IN: buffer for result
                  const unsigned char* src, // IN: buffer for source
                  int xs,                   // IN: image size
                  int ys)                   //
{
    unsigned int center,sum;
    int x,i,j;

    for (j=2;j<ys-2;j++)
    {
        for (i=2;i<xs-2;i++)
        {
            center = src[j*xs+i];
            sum = src[(j-2)*xs+(i-2)] + src[(j-2)*xs+(i-1)] + src[(j-2)*xs+i] + src[(j-2)*xs+(i+1)] + src[(j-2)*xs+(i+2)]
            +src[(j-1)*xs+(i-2)] + src[(j-1)*xs+(i-1)] + src[(j-1)*xs+i] + src[(j-1)*xs+(i+1)] + src[(j-1)*xs+(i+2)]
            +src[ j   *xs+(i-2)] + src[ j   *xs+(i-1)] +                   src[ j   *xs+(i+1)] + src[ j*   xs+(i+2)]
            +src[(j+1)*xs+(i-2)] + src[(j+1)*xs+(i-1)] + src[(j+1)*xs+i] + src[(j+1)*xs+(i+1)] + src[(j+1)*xs+(i+2)]
            +src[(j+2)*xs+(i-2)] + src[(j+2)*xs+(i-1)] + src[(j+2)*xs+i] + src[(j+2)*xs+(i+1)] + src[(j+2)*xs+(i+2)];
            x = 24*center-sum;
            if (x>255)
                x = 255;
            else
                if (x<0)
                    x = 0;
            dst[j*xs+i] = x;
        }
    }
}


//////////////////////////////////////////////////////////////////////////
///////debug functions 
//////////////////////////////////////////////////////////////////////////
//void print_image( const unsigned char* src,
//                 int xs,
//                 int ys )
//{
//    int i;
//    for ( i = 0; i < ys; ++ i )
//    {
//        int j;
//        for( j = 0; j < xs; ++ j )
//        {
//            printf( "%3d,", src[i*xs+j] );
//        }
//        printf( "\n" );
//    }
//}
//
//// compare with given data
//static void run(
//                const unsigned char* src,
//                int xs,
//                int ys)
//{
//    unsigned char *ref,*tst,*b1,*b2;
//    //////////////////////////////////////////////////////////////////////////
//    unsigned short *bufferTmp = NULL;
//    //////////////////////////////////////////////////////////////////////////
//
//    // allocate buffers
//    //////////////////////////////////////////////////////////////////////////
//    bufferTmp = (unsigned short *)malloc( (xs+1) * sizeof(*bufferTmp));
//    //////////////////////////////////////////////////////////////////////////
//
//    ref = (unsigned char*)malloc(xs*ys+xs*ys+ BUF1_SIZE(xs,ys)+BUF2_SIZE(xs,ys));
//    tst = ref+xs*ys;
//    b1 = tst+xs*ys;
//    b2 = b1+BUF1_SIZE(xs,ys);
//    // first, compare our three reference filters
//    Classic_C(ref,src,xs,ys,(int*)b1);
//    print_image( ref, xs, ys );
//    // classic versus asm
//    memset(tst,0,xs*ys);
//    printf("\n");
//    printf("\n");
//
//    TestedFilter_SSE(tst,src,xs,ys,bufferTmp, ref);
//    print_image( tst, xs, ys );
//
//
//    printf("\n");
//
//
//    //////////////////////////////////////////////////////////////////////////
//    /// It seems we forgot to free memory
//    free( ref );
//    free( bufferTmp );
//    //////////////////////////////////////////////////////////////////////////
//}
////////////////////////////////////////////////////////////////////////////
//int main1(
//          int argc,
//          char** argv)
//{
//
//    unsigned char img[] = 
//    {// 0   1   2   3   4   5   6   7   8   9   10  11  12  13  14  15 
//        0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,      //0
//        0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,      //1
//        0,  0,  0,100,  1,  2,  3,  4,  5,  6,  1,  1,  0,  0,  0,  0,      //2
//        0,  0,100, 10,  0,  0, 10,  0,222,255,255,222,  0,  0,  0,  0,      //3
//        0,  0,  0,  1,  0,  0,  0,  0,222,255,225,222,  0,  0,  0,  0,      //4
//        0,  0,  0,100,  0,  0,  0,  0,222,111,111,222,  0,  0,  0,  0,      //5
//        0,  0,  0,  0,  0,  0,  0,  0,222,222,222,222,  0,  0,  0,  0,      //6
//        0,  0,  0,  0,  0,  0,  0,  0,222,222,222,222,  0,  0,  0,  0,      //7
//        0,  0,  0,  0,  0,  0,  0,  0,  0,  7,  8,  1,  1,  1,  1,  1,      //8
//        0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,      //9
//        0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,      //10
//        0,  0,  0,  0,  0,  0,  1,  7,  0,  0,  0,  0,  0,  0,  0,  0,      //11
//        0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  3,  0,  0,  0,      //12
//        0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  2,  1,  0,  0,      //13
//        0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,      //14
//        0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0      //15
//    };
//
//    run(img,16,16);
//
//
//    return 0;
//}
