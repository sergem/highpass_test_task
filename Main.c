/*-----------------------------------------------------------------------*/
/*                                                                       */
/*      Created:  6 February 2001                                        */
/*      Modified: 6 May 2004                                             */
/*      Revision: 2.0.00                                                 */
/*      Purpose:  Testing rack for Highpass 5x5                          */
/*      Authors:                                                         */
/*        Ivan Matveev (primary)                                         */
/*        Alexander Murynin (timing in percents)                         */
/*                                                                       */
/*-----------------------------------------------------------------------*/

#include <windows.h>
#include <stdio.h>
#include <conio.h>

// reference functions and sample image
#include "hidden.h"

// size of random image
#define WIDTH   320
#define HEIGHT  240

// sizes of temporary buffers
#define BUF1_SIZE(xs,ys) (xs*4)
#define BUF2_SIZE(xs,ys) 12800

// number of passes to test filter performance
#define BENCHMARK       1000

// name of report file
#define REPORT_FILENAME "report.txt"

void TestedFilter(
	unsigned char* dst,       // IN: buffer for result
	const unsigned char* src, // IN: buffer for source
	int xs,                   // IN: image size
	int ys,
    unsigned short * bufferTmp
    );                  //

#pragma warning(disable:4996)

static void SMT_PrintToFile(
	const char* fname,
	const char* str,
	...)
{
	va_list valist;
	FILE* rf;

	va_start(valist,str);
	vprintf(str,valist);
	if (fname)
		if ((rf = fopen(fname,"at"))!=NULL)
		{
			vfprintf(rf,str,valist);
			fclose(rf);
		}
	va_end(valist);
}

// load a bitmap from file
static unsigned char* BMP_Load_Y8(
	int* xs,              // OUT: image size
	int* ys,              //
	const char* filename) // IN:  file name
{
	FILE* fp;
	BITMAPFILEHEADER bmfh;
	BITMAPINFOHEADER bmih;
	unsigned char* buf;

	// open file
	fp = fopen(filename,"rb");
	if (fp==NULL)
	{
		SMT_PrintToFile(REPORT_FILENAME,"\nCannot open file %s",filename);
		return NULL;
	}
	// read file info and verify if it is BMP
	fread(&bmfh,1,sizeof(bmfh),fp);
	if (bmfh.bfType!=((short)'B')+((short)'M')*256)
	{
		SMT_PrintToFile(REPORT_FILENAME,"\nFile %s is not Windows BMP format",filename);
		fclose(fp);
		return NULL;
	}
	// read BMP header
	fread(&bmih,1,sizeof(bmih),fp);
	// check the availability
	if ((bmih.biCompression!=0)||(bmih.biBitCount!=8))
	{
		SMT_PrintToFile(REPORT_FILENAME,"\nFile %s is not grayscale 8bpp",filename);
		fclose(fp);
		return NULL;
	}
	if (bmih.biWidth%4)
	{
		SMT_PrintToFile(REPORT_FILENAME,"\nX size of '%s' should be multiple of 4",filename);
		fclose(fp);
		return NULL;
	}
	if ((buf = (unsigned char*)malloc(bmih.biWidth*bmih.biHeight))==NULL)
	{
		SMT_PrintToFile(REPORT_FILENAME,"\nCould not allocate %d bytes",bmih.biWidth*bmih.biHeight);
		fclose(fp);
		return NULL;
	}
	// read the bitmap itself
	fseek(fp,bmfh.bfOffBits,SEEK_SET);
	fread(buf,1,bmih.biWidth*bmih.biHeight,fp);
	//
	*xs = bmih.biWidth;
	*ys = bmih.biHeight;
	fclose(fp);
	return buf;
}

static int isDiff(
	const unsigned char* tst,
	const unsigned char* ref,
	int xs,
	int ys)
{
	int i,j;

	for (i=2;i<ys-2;i++)
		for (j=2;j<xs-2;j++)
			if (tst[i*xs+j]!=ref[i*xs+j])
				return i*xs+j;
	return 0;
}

// compare with given data
static void go_compare(
	const unsigned char* src,
	int xs,
	int ys)
{
	unsigned char *ref,*tst,*b1,*b2;
    //////////////////////////////////////////////////////////////////////////
    unsigned short *bufferTmp = NULL;
    //////////////////////////////////////////////////////////////////////////
	int difidx,i;
	LARGE_INTEGER tmptime,testtime,best_asm_time,best_c_time,classic_time,freq;

	SMT_PrintToFile(REPORT_FILENAME,"\nSize of image is %dx%d.\n",xs,ys);
	// allocate buffers
    //////////////////////////////////////////////////////////////////////////
    bufferTmp = (unsigned short *)malloc( (xs+1) * sizeof(*bufferTmp));
    //////////////////////////////////////////////////////////////////////////

	ref = (unsigned char*)malloc(xs*ys+xs*ys+
		BUF1_SIZE(xs,ys)+BUF2_SIZE(xs,ys));
	tst = ref+xs*ys;
	b1 = tst+xs*ys;
	b2 = b1+BUF1_SIZE(xs,ys);
	// first, compare our three reference filters
	Classic_C(ref,src,xs,ys,(int*)b1);
	// classic versus asm
	memset(tst,0,xs*ys);
	Classic_ASM(tst,src,xs,ys,b1,xs);
	if (difidx = isDiff(ref,tst,xs,ys))
	{
		SMT_PrintToFile(REPORT_FILENAME,
			"\nFatal error.\n"
			"Two reference filter Classic_C and Classic_ASM differ at point (%d,%d)\n"
			"Classic_C=%d, Classic_ASM=%d.\n"
			"Terminating.\n",difidx%xs,difidx/xs,ref[difidx],tst[difidx]);
		exit(-1);
	}
	// classic versus best_c
	memset(tst,0,xs*ys);
	Best_C(tst,src,xs,ys,(int*)b1,b2);
	if (difidx = isDiff(ref,tst,xs,ys))
	{
		SMT_PrintToFile(REPORT_FILENAME,
			"\nFatal error.\n"
			"Two reference filter Classic_C and Best_C differ at point (%d,%d)\n"
			"Classic_C=%d, Classic_ASM=%d.\n"
			"Terminating.\n",difidx%xs,difidx/xs,ref[difidx],tst[difidx]);
		exit(-1);
	}
	// first, compare test filters
	memset(tst,0,xs*ys);
	TestedFilter(tst,src,xs,ys,bufferTmp);
	if (difidx = isDiff(ref,tst,xs,ys))
	{
		SMT_PrintToFile(REPORT_FILENAME,
			"\nERROR!\n"
			"Tested filter differs from reference at point (%d,%d)\n"
			"Test=%d, Reference=%d.\n",difidx%xs,difidx/xs,ref[difidx],tst[difidx]);
	}
	else
		SMT_PrintToFile(REPORT_FILENAME,"Tested and reference filter's results are same\n");
	// benchmark the Classic filter
	QueryPerformanceCounter(&tmptime);
	printf("\nBenchmarking Classic C filter\n");
	for (i=0;i<BENCHMARK;i++)
	{
		Classic_C(tst,src,xs,ys,(int*)b1);
		if (!(i%0xf))
			printf(".");
	}
	printf("\n");
	QueryPerformanceCounter(&classic_time);
	classic_time.QuadPart -= tmptime.QuadPart;
	// benchmark the Best_C filter
	QueryPerformanceCounter(&tmptime);
	printf("\nBenchmarking Best C filter\n");
	for (i=0;i<BENCHMARK;i++)
	{
		Best_C(tst,src,xs,ys,(int*)b1,b2);
		if (!(i%0xf))
			printf(".");
	}
	printf("\n");
	QueryPerformanceCounter(&best_c_time);
	best_c_time.QuadPart -= tmptime.QuadPart;
	// benchmark the Classic_ASM filter
	QueryPerformanceCounter(&tmptime);
	printf("\nBenchmarking Classic ASM filter\n");
	for (i=0;i<BENCHMARK;i++)
	{
		Classic_ASM(tst,src,xs,ys,b1,xs);
		if (!(i%0xf))
			printf(".");
	}
	printf("\n");
	QueryPerformanceCounter(&best_asm_time);
	best_asm_time.QuadPart -= tmptime.QuadPart;
	// benchmark the tested filter
	QueryPerformanceCounter(&tmptime);
	printf("\nBenchmarking the tested filter\n");
	for (i=0;i<BENCHMARK;i++)
	{
		TestedFilter(tst,src,xs,ys,bufferTmp);
		if (!(i%0xf))
			printf(".");
	}
	printf("\n");
	QueryPerformanceCounter(&testtime);
	testtime.QuadPart -= tmptime.QuadPart;
	//
	QueryPerformanceFrequency(&freq);
	// output benchmark results
	SMT_PrintToFile(REPORT_FILENAME,
		"Comparing time of %d passes.\n"
		"Classic C filter: %10d msec\n"
		"Best C filter:    %10d msec\n"
		"Best ASM filter:  %10d msec\n"
		"Tested filter:    %10d msec\n"
		"Taking Classic C performance time for 100%%, the timing is:\n"
		"Best C filter:    %3.2f%%\n"
		"Best ASM filter:  %3.2f%%\n"
		"Tested filter:    %3.2f%%.\n",
		BENCHMARK,
		(int)(classic_time.QuadPart*1000/freq.QuadPart),
		(int)(best_c_time.QuadPart*1000/freq.QuadPart),
		(int)(best_asm_time.QuadPart*1000/freq.QuadPart),
		(int)(testtime.QuadPart*1000/freq.QuadPart),
		((float)((best_c_time.QuadPart*100000)/classic_time.QuadPart))/1000.f,
		((float)((best_asm_time.QuadPart*100000)/classic_time.QuadPart))/1000.f,
		((float)((testtime.QuadPart*100000)/classic_time.QuadPart))/1000.f);

    //////////////////////////////////////////////////////////////////////////
    /// It seems we forgot to free memory
    free( ref );
    free( bufferTmp );
    //////////////////////////////////////////////////////////////////////////
}

void main(
	int argc,
	char** argv)
{
	int i,j;
	unsigned char* src;

	if (argc!=2)
	{
		printf(
			"\nUsage: hp5x5 <bitmap_file>\n"
			"Grayscale 8bpp Windows bitmaps are accepted.\n"
			"Width of bitmap should be a multiple of 4.\n");
		_getch();
		return;
	}
	// clear previous report file
	remove(REPORT_FILENAME);
	// raise priority
	i = SetPriorityClass(GetCurrentProcess(),REALTIME_PRIORITY_CLASS);
	if (i)
		printf("\nProcess priority raised\n");
	//== random data
	SMT_PrintToFile(REPORT_FILENAME,
		"Tests on random data.\n"
		"---------------------\n",WIDTH,HEIGHT);
	// allocate
	src = (unsigned char*)malloc(WIDTH*HEIGHT);
	// generate random data
	for (i=0;i<HEIGHT;i++)
		for (j=0;j<WIDTH;j++)
			src[i*WIDTH+j] = (char)(rand() & 0xFF);
	// compare
	go_compare(src,WIDTH,HEIGHT);
	// free
	free(src);
	//== sample data
	SMT_PrintToFile(REPORT_FILENAME,
		"\nTests on sample real image data.\n"
		"--------------------------------\n",argv[1]);
	// compare
	go_compare(imagedata,imagexs,imageys);
	//== data from BMP
	SMT_PrintToFile(REPORT_FILENAME,
		"\nTests on data from '%s' BMP file.\n"
		"---------------------------------\n",argv[1]);
	if ((src = BMP_Load_Y8(&i,&j,argv[1]))==NULL)
		return;
	// compare
	go_compare(src,i,j);
	// free
	free(src);
	SetPriorityClass(GetCurrentProcess(),NORMAL_PRIORITY_CLASS);
}

